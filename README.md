Command line chess
==================

To run
======
# in project root folder
python3 -m chess

To run the tests
================
# if you haven't already
pip3 install pytest 
# in project root folder
pytest

