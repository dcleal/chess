import copy

from chess.move import PieceMove
from chess.pieces.bishop import Bishop
from chess.pieces.knight import Knight
from chess.pieces.king import King
from chess.pieces.pawn import Pawn
from chess.pieces.piece import Empty
from chess.pieces.queen import Queen
from chess.pieces.rook import Rook
from chess.player import Player
from chess.pos import Pos

class Board():
    def __init__(self, cells):
        self.cells = cells
        self.pawnsThatMovedDoubleLastTime = { Player.WHITE : None, Player.BLACK : None }

    @classmethod
    def new(cls):
        return cls( \
            [[Rook(Player.WHITE), Knight(Player.WHITE), Bishop(Player.WHITE), Queen(Player.WHITE), 
              King(Player.WHITE), Bishop(Player.WHITE), Knight(Player.WHITE), Rook(Player.WHITE)],
             [Pawn(Player.WHITE), Pawn(Player.WHITE), Pawn(Player.WHITE), Pawn(Player.WHITE), 
              Pawn(Player.WHITE), Pawn(Player.WHITE), Pawn(Player.WHITE), Pawn(Player.WHITE)],
             [Empty(), Empty(), Empty(), Empty(), Empty(), Empty(), Empty(), Empty()],
             [Empty(), Empty(), Empty(), Empty(), Empty(), Empty(), Empty(), Empty()],
             [Empty(), Empty(), Empty(), Empty(), Empty(), Empty(), Empty(), Empty()],
             [Empty(), Empty(), Empty(), Empty(), Empty(), Empty(), Empty(), Empty()],
             [Pawn(Player.BLACK), Pawn(Player.BLACK), Pawn(Player.BLACK), Pawn(Player.BLACK), 
              Pawn(Player.BLACK), Pawn(Player.BLACK), Pawn(Player.BLACK), Pawn(Player.BLACK)],
             [Rook(Player.BLACK), Knight(Player.BLACK), Bishop(Player.BLACK), Queen(Player.BLACK), 
              King(Player.BLACK), Bishop(Player.BLACK), Knight(Player.BLACK), Rook(Player.BLACK)]])
        
    def get(self, pos):
        return self.cells[pos.row][pos.col]

    def set(self, pos, piece):
        self.cells[pos.row][pos.col] = piece

    def isOver(self, nextPlayer):
        return self.getResultString(nextPlayer) is not None

    def print(self):
        print('')
        for row in range(7, -1, -1):
            print (row + 1, end='  ')
            # TODO print white and black pieces in different Players
            print(*(piece.symbol() for piece in self.cells[row]))
        print('\n   a b c d e f g h\n')

    def getResultString(self, nextPlayer):
        if self.canMove(nextPlayer):
            return None
        elif self.isInCheck(nextPlayer):
            return "{} has won. Well done!".format(nextPlayer.oppositePlayer())
        else:
            return "{} cannot move legally but is not in check. Stalemate!".format(nextPlayer)

    def performMove(self, move):
        move.perform(self)

    def setPawnThatMovedDoubleLastTime(self, player, pos):
        self.pawnsThatMovedDoubleLastTime[player] = pos

    def getPawnThatMovedDoubleLastTime(self, player):
        return self.pawnsThatMovedDoubleLastTime[player]

    def allPositionsOwnedByPlayer(self, player):
        for row in range(0, 8):
            for col in range(0, 8):
                if self.get(Pos(col, row)).belongsTo(player):
                    yield Pos(col, row)

    def isInCheck(self, player):
        targetPos = self.posContainingKingFor(player)
        return self.isPositionInCheck(player, self.posContainingKingFor(player))

    def isPositionInCheck(self, player, pos):
        for fromPos in self.allPositionsOwnedByPlayer(player.oppositePlayer()):
            if any(toPos.equals(pos) for toPos in self.get(fromPos).positionsReachableFrom(fromPos, self)):
                return True
        return False

    def posContainingKingFor(self, player):
        return next(pos for pos in self.allPositionsOwnedByPlayer(player) if type(self.get(pos)) is King)

    def canMove(self, player):
        for fromPos in self.allPositionsOwnedByPlayer(player):
            for toPos in self.get(fromPos).positionsReachableFrom(fromPos, self):
                if not self.afterMove(PieceMove(fromPos, toPos, player)).isInCheck(player):
                    return True

    def afterMove(self, move):
        res = Board(copy.deepcopy(self.cells))
        move.perform(res)
        return res

    def positionsReachableFrom(self, fromPos, increments):
        fromPlayer = self.get(fromPos).player
        for increment in increments:
            toPos = fromPos.plus(increment)
            blocked = False
            while not blocked:
                if not self.contains(toPos) or self.get(toPos).belongsTo(fromPlayer):
                    blocked = True
                else:
                    yield toPos
                    if self.get(toPos).belongsTo(fromPlayer.oppositePlayer()):
                        blocked = True
                toPos = toPos.plus(increment)

    def contains(self, pos):
        return pos.col >= 0 and pos.col <= 7 and pos.row >= 0 and pos.row <= 7
