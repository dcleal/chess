from chess.board import Board
from chess.move import Move
from chess.player import Player

class ChessGame():
    def __init__(self):
        self.board = Board.new()
        self.nextPlayer = Player.WHITE
        self.history = []

    def run(self):
        while not self.board.isOver(self.nextPlayer):
            self.board.print()
            action = self.getAction()
            if (action.isUndo()):
                if (len(self.history) > 0):
                    self.board = self.history.pop()
                    self.nextPlayer = self.nextPlayer.oppositePlayer()
                else:
                    print("Can't undo, at beginning!")
            else:
                self.history.append(self.board)
                self.board = self.board.afterMove(action.move)
                self.nextPlayer = self.nextPlayer.oppositePlayer()
        self.board.print()
        print(self.board.getResultString(self.nextPlayer))

    def getAction(self):
        while True:
            # TODO Could suggest entering '?' to print more detailed instructions
            print("Please enter move for {} player (or 'u' to undo last move)".format(self.nextPlayer.value))
            inputString = input()
            if (inputString == 'u'):
                return UndoAction()
            else:
                move = Move.parseString(inputString, self.nextPlayer)
                if move is None:
                    print("That's not the correct format. Moves should be in the style 'a3b4', or 'O-O' or 'O-O-O' for castling, or 'u' for undo last move")
                elif not move.isLegal(self.board):
                    # TODO could ask the illegal move for a better explanation?
                    print("That's not a legal move")
                else:
                    return MoveAction(move)


class UndoAction():
    def isUndo(self):
        return True

class MoveAction():
    def __init__(self, move):
        self.move = move

    def isUndo(self):
        return False
