
from chess.pieces.king import King
from chess.pieces.piece import Empty
from chess.pieces.rook import Rook
from chess.pos import Pos

class Move():

    @classmethod
    def parseString(cls, aString, player):
        # TODO doesn't accept standard notation  https://en.wikipedia.org/wiki/Algebraic_notation_(chess)
        # (for now we insist on a3b4 style (move from a3 to b4))
        try:
            if aString.upper() == 'O-O' or aString == '0-0':
                return ShortCastlingMove(player)
            elif aString.upper() == 'O-O-O' or aString == '0-0-0':
                return LongCastlingMove(player)
            elif len(aString) == 4: 
                ints = ord(aString[:1])-97, int(aString[1:2])-1, ord(aString[2:3])-97, int(aString[3:4])-1
                if not any(i < 0 or i > 7 for i in ints):
                    return PieceMove(Pos(ints[0], ints[1]), Pos(ints[2], ints[3]), player)
        except ValueError:
            pass
        return None

    def isLegal(self, board):
        raise Exception("Subclasses should implement this")

    def perform(self, board):
        raise Exception("Subclasses should implement this")

    def equals(self, aMove):
        raise Exception("Subclasses should implement this")


class PieceMove(Move):
    def __init__(self, fromPos, toPos, player):
        self.fromPos = fromPos
        self.toPos = toPos
        self.player = player

    def __str__(self):
        return '{}=>{}'.format(self.fromPos, self.toPos)

    def isLegal(self, board):
        return board.get(self.fromPos).belongsTo(self.player) and \
               board.get(self.fromPos).canDoMove(self, board)

    def perform(self, board):
        board.get(self.fromPos).performMove(self, board)

    def equals(self, aMove):
        return instanceof(aMove, PieceMove) and aMove.fromPos.equals(self.fromPos) and \
               aMove.toPos.equals(self.toPos) and aMove.player == self.player


class CastlingMove(Move):
    def __init__(self, player):
        self.player = player

    def isLegal(self, board):
        kingMove = self.kingMove()
        rookMove = self.rookMove()
        king = board.get(kingMove.fromPos)
        rook = board.get(rookMove.fromPos)
        if king.belongsTo(self.player) and rook.belongsTo(self.player):
            return isinstance(king, King) and isinstance(rook, Rook) and \
                   not king.hasMoved and not rook.hasMoved and \
                   rook.canDoMove(rookMove, board) and self.kingCanDoCastlingMove(king, kingMove, board)

    def perform(self, board):
        kingMove = self.kingMove()
        rookMove = self.rookMove()
        board.set(rookMove.toPos, board.get(rookMove.fromPos))
        board.set(kingMove.toPos, board.get(kingMove.fromPos))
        board.set(rookMove.fromPos, Empty())
        board.set(kingMove.fromPos, Empty())
        board.get(rookMove.toPos).setHasMoved()
        board.get(kingMove.toPos).setHasMoved()

    def kingCanDoCastlingMove(self, king, move, board):
        intermediatePos = Pos((move.fromPos.col + move.toPos.col)//2, move.fromPos.row)
        intermediateMove = PieceMove(move.fromPos, intermediatePos, self.player)
        return king.canDoMove(intermediateMove, board) and \
               king.canDoMove(PieceMove(intermediatePos, move.toPos, self.player), board.afterMove(intermediateMove))

    def kingPassesThroughCheckedPosition(self, board):
        return any(board.isPositionInCheck(self.player, pos) for pos in self.kingPositions())

    def kingPositions(self):
        kingMove = self.kingMove()
        return (Pos(col, kingMove.fromPos.row) for col in 
            range(min(kingMove.fromPos.col, kingMove.toPos.col), 1+max(kingMove.fromPos.col, kingMove.toPos.col)))

    def kingMove(self):
        raise Exception("Should be implemented in subclass")

    def rookMove(self):
        raise Exception("Should be implemented in subclass")


class ShortCastlingMove(CastlingMove):
    def __str__(self):
        return 'O-O'

    def equals(self, aMove):
        return instanceof(aMove, ShortCastlingMove) and aMove.player == self.player
        
    def kingMove(self):
        return PieceMove(Pos(4, self.player.firstRow()), Pos(6, self.player.firstRow()), self.player)

    def rookMove(self):
        return PieceMove(Pos(7, self.player.firstRow()), Pos(5, self.player.firstRow()), self.player)


class LongCastlingMove(CastlingMove):
    def __str__(self):
        return 'O-O-O'

    def equals(self, aMove):
        return instanceof(aMove, LongCastlingMove) and aMove.player == self.player

    def kingMove(self):
        return PieceMove(Pos(4, self.player.firstRow()), Pos(2, self.player.firstRow()), self.player)

    def rookMove(self):
        return PieceMove(Pos(0, self.player.firstRow()), Pos(3, self.player.firstRow()), self.player)

