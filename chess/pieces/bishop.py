from chess.pieces.increments import *
from chess.pieces.piece import Piece

class Bishop(Piece):
    def __init__(self, player):
        super().__init__(player)

    def baseSymbol(self):
        return 'B'

    def positionsReachableFrom(self, fromPos, board):
        for toPos in board.positionsReachableFrom(fromPos, DIAGONAL_INCREMENTS):
            yield toPos
