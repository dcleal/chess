from chess.pos import Pos

DIAGONAL_INCREMENTS = [Pos(1, 1), Pos(1, -1), Pos(-1, 1), Pos(-1, -1)]

STRAIGHT_INCREMENTS = [Pos(1, 0), Pos(-1, 0), Pos(0, 1), Pos(0, -1)]

KNIGHT_INCREMENTS = [Pos(2, 1), Pos(2, -1), Pos(-2, 1), Pos(-2, -1),
                     Pos(1, 2), Pos(1, -2), Pos(-1, 2), Pos(-1, -2)]

