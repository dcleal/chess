from chess.pieces.piece import Piece
from chess.pos import Pos

class King(Piece):
    def __init__(self, player):
        super().__init__(player)

    def baseSymbol(self):
        return 'K'

    def canDoMove(self, move, board):
        for toPos in self.positionsReachableFrom(move.fromPos, board):
            if toPos.equals(move.toPos):
                return not board.afterMove(move).isInCheck(board.get(move.fromPos).player)
        return False

    def positionsReachableFrom(self, fromPos, board):
        for col in range(max(0, fromPos.col-1), min(8, fromPos.col+2)):
            for row in range(max(0, fromPos.row-1), min(8, fromPos.row+2)):
                toPos = Pos(col, row)
                if not toPos.equals(fromPos) and not board.get(toPos).belongsTo(self.player):
                    yield toPos