from chess.pieces.increments import *
from chess.pieces.piece import Piece

class Knight(Piece):
    def __init__(self, player):
        super().__init__(player)

    def baseSymbol(self):
        return 'N'

    def positionsReachableFrom(self, fromPos, board):
        for increment in KNIGHT_INCREMENTS:
            toPos = fromPos.plus(increment)
            if board.contains(toPos) and not board.get(toPos).belongsTo(self.player):
                yield toPos
