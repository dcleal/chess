from chess.pieces.piece import Empty
from chess.pieces.piece import Piece
from chess.pos import Pos

class Pawn(Piece):
    def __init__(self, player):
        super().__init__(player)

    def baseSymbol(self):
        return 'P'

    def positionsReachableFrom(self, fromPos, board):
        normalIncrement = self.player.pawnIncrement()
        initialRow = self.player.initialPawnRow()

        # Straight moves
        toPos = fromPos.plus(normalIncrement)
        if board.contains(toPos) and not board.get(toPos).isPiece():
            yield toPos
            if fromPos.row == initialRow:
                # possible to make two step move
                toPos = toPos.plus(normalIncrement)
                if not board.get(toPos).isPiece():
                    yield toPos
        
        # Taking moves
        takingPositions = [fromPos.plus(normalIncrement).plus(Pos(-1, 0)), 
                           fromPos.plus(normalIncrement).plus(Pos( 1, 0))]
        for toPos in takingPositions:
            if board.contains(toPos) and board.get(toPos).belongsTo(self.player.oppositePlayer()):
                yield toPos

        # En passant moves
        enPassantTargetPos = board.getPawnThatMovedDoubleLastTime(self.player.oppositePlayer())
        if enPassantTargetPos is not None and fromPos.row == self.player.enPassantStartRow() and \
           abs(fromPos.col - enPassantTargetPos.col) == 1:
           yield toPos


    # Override standard performMove to handle en passant, and to record double moves
    def performMove(self, move, board):
        if self.performingDoubleMove(move):
            board.setPawnThatMovedDoubleLastTime(self.player, move.toPos)
        
        if self.performingEnPassant(move, board):
            board.set(move.toPos.minus(self.player.pawnIncrement()), Empty())

        super().performMove(move, board)
        
        
    def performingDoubleMove(self, move):
        return move.fromPos.row == self.player.initialPawnRow() and \
               move.toPos.row != move.fromPos.plus(self.player.pawnIncrement()).row

    def performingEnPassant(self, move, board):
        return move.fromPos.col != move.toPos.col and not board.get(move.toPos).isPiece()
