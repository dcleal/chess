from itertools import chain

from chess.pos import Pos

class Piece:
    def __init__(self, player):
        self.player = player
        self.hasMoved = False

    def isPiece(self):
        return True

    def symbol(self):
        return self.player.symbolBasedOn(self.baseSymbol())

    def performMove(self, move, board):
        board.set(move.toPos, self)
        board.set(move.fromPos, Empty())
        self.hasMoved = True


    def belongsTo(self, player):
        return self.player == player

    def canDoMove(self, move, board):
        for toPos in self.positionsReachableFrom(move.fromPos, board):
            if toPos.equals(move.toPos):
                return not board.afterMove(move).isInCheck(board.get(move.fromPos).player)
        return False

    def setHasMoved(self):
        self.hasMoved = True

    def positionsReachableFrom(self, fromPos, board):
        # Subclasses should lazily generate all the reachable cells for a given piece
        raise Exception("should be overridden in subclass")

    def baseSymbol(self):
        raise "Should be overridden in subclass"


class Empty(Piece):
    def __init__(self):
        self.player = None

    def symbol(self):
        return '.'

    def isPiece(self):
        return False
        
    def canMove(self, move, board):
        return False

    def belongsTo(self, player):
        return False


