from itertools import chain

from chess.pieces.increments import *
from chess.pieces.piece import Piece

class Queen(Piece):
    def __init__(self, player):
        super().__init__(player)

    def baseSymbol(self):
        return 'Q'

    def positionsReachableFrom(self, fromPos, board):
        for x in board.positionsReachableFrom(fromPos, chain(DIAGONAL_INCREMENTS, STRAIGHT_INCREMENTS)):
            yield x
