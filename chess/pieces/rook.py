from chess.pieces.increments import *
from chess.pieces.piece import Piece

class Rook(Piece):
    def __init__(self, player):
        super().__init__(player)

    def baseSymbol(self):
        return 'R'

    def positionsReachableFrom(self, fromPos, board):
        for toPos in board.positionsReachableFrom(fromPos, STRAIGHT_INCREMENTS):
            yield toPos