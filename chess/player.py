from enum import Enum

from chess.pos import Pos

class Player(Enum):
    WHITE = 'White'
    BLACK = 'Black'

    def oppositePlayer(self):
        return Player.WHITE if self == Player.BLACK else Player.BLACK

    def firstRow(self):
        return 0 if self == Player.WHITE else 7

    def pawnIncrement(self):
        return Pos(0, 1) if self == Player.WHITE else Pos(0, -1)

    def initialPawnRow(self):
        return 1 if self == Player.WHITE else 6

    def symbolBasedOn(self, base):
        return base if self == Player.WHITE else base.lower()

    def enPassantStartRow(self):
        return 4 if self == Player.WHITE else 3

