class Pos():
    def __init__(self, col, row):
        self.col = col
        self.row = row

    def __str__(self):
        return '[{}, {}]'.format(self.col, self.row)

    def plus(self, otherPos):
        return Pos(self.col + otherPos.col, self.row + otherPos.row)

    def minus(self, otherPos):
        return Pos(self.col - otherPos.col, self.row - otherPos.row)

    def equals(self, otherPos):
        return self.row == otherPos.row and self.col == otherPos.col
