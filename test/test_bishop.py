import unittest

from .context import chess

from chess.board import Board
from chess.move import Move
from chess.pieces.bishop import Bishop
from chess.pieces.king import King
from chess.pieces.rook import Rook
from chess.pieces.piece import Empty
from chess.player import Player
from chess.pos import Pos

rookW = Rook(Player.WHITE)
rookB = Rook(Player.BLACK)
bishW = Bishop(Player.WHITE)
bishB = Bishop(Player.BLACK)
kingW = King(Player.WHITE)
kingB = King(Player.BLACK)
empty = Empty()

class TestBishop(unittest.TestCase):
    def aTestBoard(self):
        return Board( \
            [[rookW, rookW, bishW, bishW, kingW, bishW, empty, rookW],
             [rookW, empty, empty, empty, empty, empty, empty, rookW],
             [rookW, empty, empty, rookW, empty, empty, rookW, rookW],
             [bishB, empty, empty, empty, empty, empty, empty, rookW],
             [rookW, empty, empty, empty, empty, empty, empty, bishB],
             [rookW, empty, empty, rookW, empty, empty, empty, rookW],
             [rookW, empty, empty, empty, empty, empty, empty, rookW],
             [rookW, rookW, rookW, bishW, empty, kingB, rookB, rookW]]) 

    def test_bishop(self):
        board = self.aTestBoard()
        board.print()
        self.assertTrue(bishW.canDoMove(Move.parseString('d1h5', Player.WHITE), board))
        self.assertTrue(bishB.canDoMove(Move.parseString('h5d1', Player.BLACK), board))
        self.assertTrue(bishW.canDoMove(Move.parseString('d1a4', Player.WHITE), board))
        self.assertTrue(bishB.canDoMove(Move.parseString('a4d1', Player.BLACK), board))

        self.assertFalse(bishW.canDoMove(Move.parseString('b1d4', Player.WHITE), board))
        self.assertFalse(bishW.canDoMove(Move.parseString('e1h4', Player.WHITE), board))