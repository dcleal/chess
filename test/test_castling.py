import unittest

from .context import chess

from chess.board import Board
from chess.move import Move
from chess.pieces.king import King
from chess.pieces.rook import Rook
from chess.player import Player
from chess.pos import Pos

class TestCastling(unittest.TestCase):
    def test_short_castling_legality(self):
        board = Board.new()
        Move.parseString('g2g3', Player.WHITE).perform(board) # pawns
        Move.parseString('g7g6', Player.BLACK).perform(board)
        Move.parseString('f1h3', Player.WHITE).perform(board) # bishops
        Move.parseString('f8h6', Player.BLACK).perform(board)
        whiteShortCastlingMove = Move.parseString('O-O', Player.WHITE)
        blackShortCastlingMove = Move.parseString('O-O', Player.BLACK)
        self.assertFalse(whiteShortCastlingMove.isLegal(board)) # knights in way
        self.assertFalse(blackShortCastlingMove.isLegal(board))
        Move.parseString('g1f3', Player.WHITE).perform(board) # knights
        Move.parseString('g8f6', Player.BLACK).perform(board)
        self.assertTrue(whiteShortCastlingMove.isLegal(board))
        self.assertTrue(blackShortCastlingMove.isLegal(board))
        Move.parseString('e1f1', Player.WHITE).perform(board) # kings to the right...
        Move.parseString('e8f8', Player.BLACK).perform(board)
        Move.parseString('f1e1', Player.WHITE).perform(board) # ...and back
        Move.parseString('f8e8', Player.BLACK).perform(board)
        self.assertFalse(whiteShortCastlingMove.isLegal(board)) # kings have moved now
        self.assertFalse(blackShortCastlingMove.isLegal(board))


    def test_short_castling_moves(self):
        board = Board.new()
        Move.parseString('g2g3', Player.WHITE).perform(board) # pawns
        Move.parseString('g7g6', Player.BLACK).perform(board)
        Move.parseString('f1h3', Player.WHITE).perform(board) # bishops
        Move.parseString('f8h6', Player.BLACK).perform(board)
        Move.parseString('g1f3', Player.WHITE).perform(board) # knights
        Move.parseString('g8f6', Player.BLACK).perform(board)
        whiteShortCastlingMove = Move.parseString('O-O', Player.WHITE)
        blackShortCastlingMove = Move.parseString('O-O', Player.BLACK)
        self.assertTrue(whiteShortCastlingMove.isLegal(board))
        self.assertTrue(blackShortCastlingMove.isLegal(board))
        whiteShortCastlingMove.perform(board)
        self.assertTrue(isinstance(board.get(Pos(6, 0)), King))
        self.assertTrue(isinstance(board.get(Pos(5, 0)), Rook))
        self.assertFalse(board.get(Pos(7, 0)).isPiece())
        self.assertFalse(board.get(Pos(4, 0)).isPiece())

        blackShortCastlingMove.perform(board)
        self.assertTrue(isinstance(board.get(Pos(6, 7)), King))
        self.assertTrue(isinstance(board.get(Pos(5, 7)), Rook))
        self.assertFalse(board.get(Pos(7, 7)).isPiece())
        self.assertFalse(board.get(Pos(4, 7)).isPiece())

    def test_long_castling_legality(self):
        board = Board.new()
        Move.parseString('e2e3', Player.WHITE).perform(board) # pawns
        Move.parseString('e7e6', Player.BLACK).perform(board)
        Move.parseString('c1e3', Player.WHITE).perform(board) # bishops
        Move.parseString('c8e6', Player.BLACK).perform(board)
        Move.parseString('b1a3', Player.WHITE).perform(board) # knights
        Move.parseString('b8a6', Player.BLACK).perform(board)
        whiteLongCastlingMove = Move.parseString('0-0-0', Player.WHITE)
        blackLongCastlingMove = Move.parseString('0-0-0', Player.BLACK)
        self.assertFalse(whiteLongCastlingMove.isLegal(board)) # queens in way
        self.assertFalse(blackLongCastlingMove.isLegal(board))
        Move.parseString('d1d2', Player.WHITE).perform(board) # queens
        Move.parseString('d8d7', Player.BLACK).perform(board)
        self.assertTrue(whiteLongCastlingMove.isLegal(board))
        self.assertTrue(blackLongCastlingMove.isLegal(board))
        Move.parseString('e1d1', Player.WHITE).perform(board) # kings to the right...
        Move.parseString('e8d8', Player.BLACK).perform(board)
        Move.parseString('d1e1', Player.WHITE).perform(board) # kings to the right...
        Move.parseString('d8e8', Player.BLACK).perform(board)
        self.assertFalse(whiteLongCastlingMove.isLegal(board)) # kings have moved now
        self.assertFalse(blackLongCastlingMove.isLegal(board))


    def test_long_castling_moves(self):
        board = Board.new()
        Move.parseString('e2e3', Player.WHITE).perform(board) # pawns
        Move.parseString('e7e6', Player.BLACK).perform(board)
        Move.parseString('c1e3', Player.WHITE).perform(board) # bishops
        Move.parseString('c8e6', Player.BLACK).perform(board)
        Move.parseString('b1a3', Player.WHITE).perform(board) # knights
        Move.parseString('b8a6', Player.BLACK).perform(board)
        Move.parseString('d1d2', Player.WHITE).perform(board) # queens
        Move.parseString('d8d7', Player.BLACK).perform(board)
        whiteLongCastlingMove = Move.parseString('0-0-0', Player.WHITE)
        blackLongCastlingMove = Move.parseString('0-0-0', Player.BLACK)
        self.assertTrue(whiteLongCastlingMove.isLegal(board))
        self.assertTrue(blackLongCastlingMove.isLegal(board))
        
        whiteLongCastlingMove.perform(board)
        self.assertTrue(isinstance(board.get(Pos(2, 0)), King))
        self.assertTrue(isinstance(board.get(Pos(3, 0)), Rook))
        self.assertFalse(board.get(Pos(0, 0)).isPiece())
        self.assertFalse(board.get(Pos(4, 0)).isPiece())

        blackLongCastlingMove.perform(board)
        self.assertTrue(isinstance(board.get(Pos(2, 7)), King))
        self.assertTrue(isinstance(board.get(Pos(3, 7)), Rook))
        self.assertFalse(board.get(Pos(0, 7)).isPiece())
        self.assertFalse(board.get(Pos(4, 7)).isPiece())

    def test_cannot_castle_through_check(self):
        board = Board.new()
        Move.parseString('g2g3', Player.WHITE).perform(board) # pawns
        Move.parseString('g7g6', Player.BLACK).perform(board)
        Move.parseString('f1h3', Player.WHITE).perform(board) # bishops
        Move.parseString('f8h6', Player.BLACK).perform(board)
        Move.parseString('g1f3', Player.WHITE).perform(board) # knights
        Move.parseString('g8f6', Player.BLACK).perform(board)
        Move.parseString('O-O', Player.WHITE).perform(board)
        Move.parseString('a7a6', Player.BLACK).perform(board)
        Move.parseString('f3d4', Player.WHITE).perform(board)
        Move.parseString('a6a5', Player.BLACK).perform(board)
        Move.parseString('d4e6', Player.WHITE).perform(board)
        board.print()
        blackShortCastlingMove = Move.parseString('O-O', Player.BLACK)
        self.assertFalse(blackShortCastlingMove.isLegal(board)) # Knight checks intermediate square!
        Move.parseString('a5a4', Player.BLACK).perform(board)
        Move.parseString('e6d4', Player.WHITE).perform(board)
        self.assertTrue(blackShortCastlingMove.isLegal(board)) # Knight moved away!
        

        