import unittest

from .context import chess

from chess.board import Board
from chess.move import Move
from chess.player import Player
from chess.pos import Pos

class TestKing(unittest.TestCase):
    def test_king_sideways(self):
        board = Board.new()
        Move.parseString('g2g3', Player.WHITE).perform(board) # pawns
        Move.parseString('g7g6', Player.BLACK).perform(board)
        Move.parseString('f1h3', Player.WHITE).perform(board) # bishops
        Move.parseString('f8h6', Player.BLACK).perform(board)
        Move.parseString('g1f3', Player.WHITE).perform(board) # knights
        Move.parseString('g8f6', Player.BLACK).perform(board)
        
        whiteKing = board.get(Pos(4, 0))
        reachable = list(whiteKing.positionsReachableFrom(Pos(4, 0), board))
        self.assertEqual(len(reachable), 1)
        self.assertTrue(Pos(5, 0).equals(reachable[0]))
        
        blackKing = board.get(Pos(4, 7))
        reachable = list(blackKing.positionsReachableFrom(Pos(4, 7), board))
        self.assertEqual(len(reachable), 1)
        self.assertTrue(Pos(5, 7).equals(reachable[0]))

        x = board.afterMove(Move.parseString('e1f1', Player.WHITE))
        self.assertFalse(x.isInCheck(Player.WHITE))

        x = board.afterMove(Move.parseString('e8f8', Player.BLACK))
        self.assertFalse(x.isInCheck(Player.BLACK))