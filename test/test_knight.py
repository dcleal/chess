import unittest

from .context import chess

from chess.board import Board
from chess.move import Move
from chess.pieces.knight import Knight
from chess.pieces.king import King
from chess.pieces.piece import Empty
from chess.pieces.rook import Rook
from chess.player import Player
from chess.pos import Pos

class TestKnightMove(unittest.TestCase):
    def aTestBoard(self):
        rookW = Rook(Player.WHITE)
        rookB = Rook(Player.BLACK)
        knigW = Knight(Player.WHITE)
        kingW = King(Player.WHITE)
        kingB = King(Player.BLACK)
        empty = Empty()
        return Board( \
            [[rookW, rookW, rookW, rookW, kingW, rookW, rookW, rookW],
             [rookW, empty, empty, empty, empty, empty, empty, rookW],
             [rookW, empty, empty, rookW, empty, empty, rookW, rookW],
             [rookW, empty, empty, empty, empty, empty, empty, rookW],
             [rookW, empty, empty, knigW, empty, empty, empty, rookW],
             [rookW, empty, empty, rookW, rookW, rookW, empty, rookW],
             [rookW, empty, empty, empty, empty, empty, empty, rookW],
             [rookW, rookW, rookW, rookB, kingB, rookB, rookW, rookW]]) 

    def test_move(self):
        board = self.aTestBoard()
        board.print()
        self.assertTrue(Knight(Player.WHITE).canDoMove(Move.parseString('d5b6', Player.WHITE), board))
        self.assertTrue(Knight(Player.WHITE).canDoMove(Move.parseString('d5e3', Player.WHITE), board))
        self.assertTrue(Knight(Player.WHITE).canDoMove(Move.parseString('d5e7', Player.WHITE), board))

        self.assertFalse(Knight(Player.WHITE).canDoMove(Move.parseString('d5f7', Player.WHITE), board))
        self.assertFalse(Knight(Player.WHITE).canDoMove(Move.parseString('d5e5', Player.WHITE), board))