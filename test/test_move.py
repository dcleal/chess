import unittest

from .context import chess

from chess.move import Move
from chess.player import Player

class TestMove(unittest.TestCase):
    def test_parse(self):
        self.assertIsNone(Move.parseString('a1b2c3', Player.WHITE), "too long")
        self.assertIsNone(Move.parseString('a1b', Player.WHITE), "too short")
        self.assertIsNone(Move.parseString('a9b1', Player.WHITE), "number out of bounds")
        self.assertIsNone(Move.parseString('a1i1', Player.WHITE), "letter out of bounds")
        self.assertIsNone(Move.parseString('', Player.WHITE), "empty")

        aMove = Move.parseString('b6d3', Player.WHITE)
        self.assertEqual(aMove.fromPos.col, 1)
        self.assertEqual(aMove.fromPos.row, 5)
        self.assertEqual(aMove.toPos.col, 3)
        self.assertEqual(aMove.toPos.row, 2)
        self.assertEqual(aMove.player, Player.WHITE)