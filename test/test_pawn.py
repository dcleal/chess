import unittest

from .context import chess

from chess.board import Board
from chess.move import Move
from chess.pieces.bishop import Bishop
from chess.pieces.knight import Knight
from chess.pieces.king import King
from chess.pieces.pawn import Pawn
from chess.pieces.piece import Empty
from chess.pieces.rook import Rook
from chess.player import Player
from chess.pos import Pos

class TestPawnMove(unittest.TestCase):
    def aTestBoard(self):
        bishW = Bishop(Player.WHITE)
        rookW = Rook(Player.WHITE)
        rookB = Rook(Player.BLACK)
        knigW = Knight(Player.WHITE)
        kingW = King(Player.WHITE)
        kingB = King(Player.BLACK)
        pawnW = Pawn(Player.WHITE)
        pawnB = Pawn(Player.BLACK)
        empty = Empty()
        return Board( \
            [[rookW, rookW, rookW, rookW, kingW, rookW, rookW, rookW],
             [rookW, pawnW, empty, pawnW, empty, empty, empty, rookW],
             [rookW, empty, empty, rookW, empty, empty, rookW, rookW],
             [rookW, empty, empty, empty, empty, empty, empty, rookW],
             [rookW, empty, empty, knigW, empty, empty, empty, rookW],
             [rookW, empty, empty, rookW, bishW, rookW, empty, rookW],
             [rookW, pawnB, empty, pawnB, empty, empty, empty, rookW],
             [rookW, rookW, rookW, rookB, kingB, rookB, rookW, rookW]]) 

    def test_move(self):
        board = self.aTestBoard()
        board.print()
        self.assertTrue(Pawn(Player.WHITE).canDoMove(Move.parseString('b2b3', Player.WHITE), board))
        self.assertTrue(Pawn(Player.WHITE).canDoMove(Move.parseString('b2b4', Player.WHITE), board))
        self.assertTrue(Pawn(Player.BLACK).canDoMove(Move.parseString('b7b6', Player.BLACK), board))
        self.assertTrue(Pawn(Player.BLACK).canDoMove(Move.parseString('b7b5', Player.BLACK), board))


        self.assertFalse(Pawn(Player.WHITE).canDoMove(Move.parseString('b2b5', Player.WHITE), board))
        self.assertFalse(Pawn(Player.WHITE).canDoMove(Move.parseString('d2d4', Player.WHITE), board))
        self.assertFalse(Pawn(Player.BLACK).canDoMove(Move.parseString('b7b4', Player.BLACK), board))
        self.assertFalse(Pawn(Player.BLACK).canDoMove(Move.parseString('d7d5', Player.BLACK), board))


    def test_en_passant_move(self):
        board = Board.new()
        Move.parseString('c2c4', Player.WHITE).perform(board)
        Move.parseString('g7g6', Player.BLACK).perform(board)
        Move.parseString('c4c5', Player.WHITE).perform(board)
        Move.parseString('d7d5', Player.BLACK).perform(board)
        enPassantMove = Move.parseString('c5d6', Player.WHITE)
        self.assertTrue(enPassantMove.isLegal(board))
        board.print()
        enPassantMove.perform(board)
        board.print()
        self.assertFalse(board.get(Pos(3, 4)).isPiece()) # captured pawn square

    def test_en_passant_move_only_works_first_time(self):
        board = Board.new()
        Move.parseString('c2c4', Player.WHITE).perform(board)
        Move.parseString('g7g6', Player.BLACK).perform(board)
        Move.parseString('c4c5', Player.WHITE).perform(board)
        Move.parseString('d7d5', Player.BLACK).perform(board)
        enPassantMove = Move.parseString('c5d6', Player.WHITE)
        self.assertTrue(enPassantMove.isLegal(board))
        Move.parseString('g2g3', Player.WHITE).perform(board)
        Move.parseString('g7g6', Player.WHITE).perform(board)
        self.assertTrue(enPassantMove.isLegal(board))