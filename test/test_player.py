import unittest

from .context import chess

from chess.player import Player

class TestPlayer(unittest.TestCase):

    def test_opposite(self):
        self.assertEqual(Player.WHITE.oppositePlayer(), Player.BLACK)
        self.assertEqual(Player.BLACK.oppositePlayer(), Player.WHITE)

if __name__ == '__main__':
    unittest.main()