import unittest

from .context import chess

from chess.pos import Pos

class TestPos(unittest.TestCase):

    def test_addition(self):
        pos = Pos(2, 3).plus(Pos(3, -1))
        self.assertEqual(5, pos.col)
        self.assertEqual(pos.row, 2)

    def test_equality(self):
        self.assertTrue(Pos(2, 3).equals(Pos(2, 3)))
        self.assertFalse(Pos(2, 3).equals(Pos(2, 4)))
        self.assertFalse(Pos(2, 3).equals(Pos(1, 3)))

if __name__ == '__main__':
    unittest.main()