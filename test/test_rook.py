import unittest

from .context import chess

from chess.board import Board
from chess.move import Move
from chess.pieces.bishop import Bishop
from chess.pieces.king import King
from chess.pieces.piece import Empty
from chess.pieces.rook import Rook
from chess.pos import Pos
from chess.player import Player

kingW = King(Player.WHITE)
kingB = King(Player.BLACK)
rookW = Rook(Player.WHITE)
rookB = Rook(Player.BLACK)
bishW = Bishop(Player.WHITE)
bishB = Bishop(Player.BLACK)
empty = Empty()

class TestRook(unittest.TestCase):
    def aTestBoard(self):
        return Board( \
            [[rookW, rookW, rookW, rookW, kingW, rookW, rookW, rookW],
             [rookW, empty, empty, empty, empty, empty, empty, rookW],
             [rookW, empty, empty, rookW, empty, empty, rookW, rookW],
             [bishB, empty, empty, empty, empty, empty, empty, rookW],
             [rookW, empty, empty, empty, empty, empty, empty, bishB],
             [rookW, empty, empty, rookW, empty, empty, empty, rookW],
             [rookW, empty, empty, empty, empty, empty, empty, rookW],
             [rookW, rookW, rookW, bishW, kingB, rookB, rookW, rookW]]) 

    def test_rook(self):
        board = self.aTestBoard()
        board.print()
        self.assertTrue(bishW.canDoMove(Move.parseString('d1h5', Player.WHITE), board))
        self.assertTrue(bishB.canDoMove(Move.parseString('h5d1', Player.BLACK), board))
        self.assertTrue(bishW.canDoMove(Move.parseString('d1a4', Player.WHITE), board))
        self.assertTrue(bishB.canDoMove(Move.parseString('a4d1', Player.BLACK), board))

        self.assertFalse(bishW.canDoMove(Move.parseString('b1e4', Player.WHITE), board))
        self.assertFalse(bishW.canDoMove(Move.parseString('e1h4', Player.WHITE), board))

    def test_row(self):
        board = self.aTestBoard()
        board.print()
        self.assertTrue(rookW.canDoMove(Move.parseString('a5h5', Player.WHITE), board))
        self.assertFalse(rookW.canDoMove(Move.parseString('a6h6', Player.WHITE), board))
