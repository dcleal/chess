import unittest

from .context import chess

from chess.board import Board
from chess.move import Move
from chess.player import Player

class TestSpecifPositions(unittest.TestCase):
    def test_a_pos(self):
        board = Board.new()
        Move.parseString('d2d4', Player.WHITE).perform(board)
        Move.parseString('e7e6', Player.BLACK).perform(board)
        Move.parseString('g1h3', Player.WHITE).perform(board)
        Move.parseString('f8b4', Player.BLACK).perform(board)
        Move.parseString('g2g3', Player.WHITE).perform(board)
        Move.parseString('b4c3', Player.BLACK).perform(board)
        board.print()
        self.assertTrue(Move.parseString('b2c3', Player.WHITE).isLegal(board))


    def foolsmate(self):
        board = Board.new()
        Move.parseString('f2f3', Player.WHITE).perform(board)
        Move.parseString('e7e6', Player.BLACK).perform(board)
        Move.parseString('g2g4', Player.WHITE).perform(board)
        Move.parseString('d8h4', Player.BLACK).perform(board)
        board.print()
        self.assertTrue(board.isOver(Player.WHITE))
