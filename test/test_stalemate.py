import unittest

from .context import chess

from chess.board import Board
from chess.move import Move
from chess.pieces.king import King
from chess.pieces.pawn import Pawn
from chess.pieces.piece import Empty
from chess.pieces.queen import Queen
from chess.pieces.rook import Rook
from chess.pos import Pos
from chess.player import Player

rookB = Rook(Player.BLACK)
pawnW = Pawn(Player.WHITE)
pawnB = Pawn(Player.BLACK)
kingW = King(Player.WHITE)
kingB = King(Player.BLACK)
queeB = Queen(Player.BLACK)
empty = Empty()

class TestStalemate(unittest.TestCase):
    def aTestBoard(self):
        return Board( \
            [[empty, empty, empty, kingW, empty, empty, empty, empty],
             [empty, empty, empty, empty, empty, empty, empty, queeB],
             [empty, empty, empty, empty, empty, empty, empty, empty],
             [pawnW, empty, rookB, empty, rookB, empty, empty, empty],
             [empty, empty, empty, empty, empty, empty, empty, empty],
             [pawnB, empty, empty, empty, empty, empty, empty, empty],
             [empty, empty, empty, empty, empty, empty, empty, empty],
             [empty, empty, empty, kingB, empty, empty, empty, empty]]) 

    def test_stalemate(self):
        board = self.aTestBoard()
        self.assertFalse(board.isOver(Player.WHITE))
        board.print()
        Move.parseString('a4a5', Player.WHITE).perform(board)
        board.print()
        Move.parseString('e4e5', Player.WHITE).perform(board)
        board.print()
        self.assertTrue(board.isOver(Player.WHITE))
        self.assertFalse(board.isInCheck(Player.WHITE))
        